var createError = require('http-errors');
var express = require('express');
var express_locale = require('express-locale');
var bodyParser = require('body-parser');
var logger = require('morgan');
var boom = require('express-boom');
var passport = require('passport');
var cors = require('cors')

const invoices = require('./modules/invoices/routes')

var app = express();


//var envs = process.env;
//console.log("Envs:", envs);

//--------------------------------------\\
// work arounds over cyclic depenedencies!
// global.events = events;
// global.eventsController = eventsController;

// global.searchController = searchController;
//--------------------------------------//
//--------------------------------------//

// global.__basedir = __dirname;
// global.__DOMAIN = process.env.DOMAIN;

// global.PROFIT_PERCENTAGE = 0.2;
// global.GLOBAL_MIN_VAL = 10;
// global.PRICE_PER_SEG = 2;
// global.MULIPLIER_VAL = 10;
// global.SEGMENT_VAL = 5000;

// doc_types.findAll().then(docs => {
//   global.DOC_TYPES = docs;
// });
// carsBLL.getAllCollections((err, collections) => {
//   global.genders = collections.genders;
//   global.cars = collections.cars;
//   global.Areas = collections.Areas;
//   global.Banks = collections.Banks;
//   global.Nationalities = collections.Nationalities;
// });

// carsBLL.getAllCities((err, cities) => {
//   global.cities = cities;
// });

// global.system_config = JSON.parse(fs.readFileSync(global.__basedir + process.env.CONFIG_DIR));

app.use(cors())
app.use(logger('short'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(boom());
app.use(express_locale());
app.use(passport.initialize());

// require('./middlewares/passport').jwtStrategy(passport);
// require('./middlewares/passport').localStrategy(passport);
// require('./middlewares/passport').adminStrategy(passport);
// require('./middlewares/passport').googleStrategy(passport);


// const authGuard = passport.authenticate('jwt', { session: false });

// app.use('/docs/terms.html', function (req, res) {
//   console.log('EULA Requested!');
//   var _p = path.join(__dirname + '/docs/terms.html');`
//   res.sendFile(_p);
// });

// var staticPath = path.join(__dirname, process.env.UPLOAD_DIR);
// app.use('/api/assets/', express.static(staticPath));

// var dashboardPath = path.join(__dirname, '/client/dist/star-admin-angular');
// console.log(dashboardPath);
// app.use(subdomain('dashboard', express.static(dashboardPath)));

//App.Use
app.use('/invoices',invoices)

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // return the error page
  res.status(err.status || 500);
  res.end();
});

// eventsController.loadEventsFromDatabase();

// var recurranceRule = new schedule.RecurrenceRule();
// recurranceRule.date = global.system_config.monthly_closing_day || 1;
// recurranceRule.hour = global.system_config.monthly_closing_hour || 4;
// //recurranceRule.date = 19;
// //recurranceRule.hour = 23;
// //recurranceRule.minute = 50;

// eventsController.scheduleRecurringEvent(recurranceRule, events.onMonthlyClosing.EventBind());

module.exports = app;
