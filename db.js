const mongoose = require('mongoose')
mongoose.connect(process.env.mongoDbURL, {
  useMongoClient: true
})
mongoose.connection.on('connected', () => {
  console.log(` | Database Is Live and Connected |\n ================================== \n`)
})
mongoose.connection.on('error', (error) => {
  console.log('Something worng!!', error)
})

module.exports = sequelize;