const invoicesDal = require('./dal')
const helper = require('../helper')

module.exports = {

uploadInvoicesFile :(req, callback) => {
    console.log(req.file)
    helper.parseFile(req.file.path, (err, data) => {
        if (err) {
            return callback(true, data)
        }
        else {
            invoicesDal.removeAllFromCollection((err,res) => {
                if(err){
                    return callback(true, res)
                }
                else{
                    invoicesDal.insertFile(data , (err, res)=>{
                        if(err){
                            return callback(true, res)
                        }
                        else{
                            return callback(false , res)
                        }
                    })
                }
            })
        }

    })
},

}