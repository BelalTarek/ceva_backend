const mongoose = require('mongoose')
const invoicesSchema = mongoose.Schema({
    Sold_To: {
        type: String
    },
    Sold_To_Name: {
        type: String
    },
    GL_Date: {
        type: String
    },
    Order_Number: {
        type: String
    },
    Document_Number: {
        type: String
    },
    Description1: {
        type: String
    },
    Extended_Amount: {
        type: String
    },
    Quantity: {
        type: String
    },
    Unit_Price: {
        type: String
    },
    Customer_PO: {
        type: String
    },
    Or_Ty: {
        type: String
    },
    fileVersion: {
        versionUrl: {
            type: String
        },
        versionNumber: {
            type: Number
        },
    }
})
			  					


const invoices = module.exports = mongoose.model('invoices', invoicesSchema)