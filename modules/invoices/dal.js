const invoicesModel = require('./model')

module.exports = {

    insertFile: (data, callback) => {
        invoicesModel.insertMany(data, (err, res) => {
            if (err || !res) {
                return callback(true, err)
            }
            else {
                return callback(false, res)
            }
        })
    },
    removeAllFromCollection: (callback) => {
        invoicesModel.deleteMany({}, (err, res) => {
            if (err) {
                return callback(true, err)
            }
            else {
                return callback(false, res)
            }
        })
    },

}