var express = require('express');
var router = express.Router();

//require controllers
// var middlewares = require('../middlewares');
var invoicesController = require('./controllers');
// var jwt = require('express-jwt');

var multer  = require('multer')
var upload = multer({ dest: 'uploads/' })


router.post('/upload-invoices',upload.single('file'), (req, res) => {
    console.log(req.file)
    invoicesController.uploadInvoicesFile(req, (err , data) => {
        if(!err){
            res.json({
                success: true,
                data:data
            })
        }
        else{
            res.json({
                success: false,
                err:data.message
            })
        }
        
    })
});

module.exports = router;