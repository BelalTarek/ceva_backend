const XLSX = require('xlsx');

module.exports = {
    
    parseFile: (filePath, callback) => {
        try {
            var workbook = XLSX.readFile(filePath);
            var sheet_name_list = workbook.SheetNames;
            var data = XLSX.utils.sheet_to_json(workbook.Sheets[sheet_name_list[0]])
        }
        catch (err) {
            return callback(true, err)
        }
        return callback(false, data)


    },

}