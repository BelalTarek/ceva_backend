// const passportJWT = require("passport-jwt");
// var LocalStrategy = require('passport-local').Strategy;
// const GoogleTokenStrategy = require('passport-google-token').Strategy;
// const PNF = require('google-libphonenumber').PhoneNumberFormat;
// const phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();
// const JWTStrategy   = passportJWT.Strategy;
// const ExtractJWT = passportJWT.ExtractJwt;

// const Users = require('../db/models/users');
// const reps = require('../db/models/representatives');
// const admins = require('../db/models/admins');

// module.exports = {
//     jwtStrategy: function(passport) { 
//     passport.use(new JWTStrategy({
//     jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
//     secretOrKey   : process.env.JWT_ENCRYPTION
//     }, (jwt_payload, done) => {
//         console.log(jwt_payload);
//         if (jwt_payload.is_rep == true) {
//             reps.findByPk(jwt_payload.user_id).then(rep => {
//                 if(rep) {
//                     return done(null, rep);
//                 }else{
//                     return done(null, null);
//                 }
//             });
//         } else if (jwt_payload.is_admin == true) {
//             admins.findByPk(jwt_payload.user_id).then(admin => {
//                 if(admin) {
//                     return done(null, admin);
//                 }else{
//                     return done(null, null);
//                 }
//             });
//         } else {
//             Users.findByPk(jwt_payload.user_id).then(user => {
//                 if(user && !user.blocked) {
//                     return done(null, user);
//                 }else{
//                     return done(null, null, {
//                         message: "This user is blocked from using this service!"
//                     });
//                 }
//             });
//         }
//     }));
// }, 

// googleStrategy: function(passport) {
//     passport.use(new GoogleTokenStrategy({
//         clientID: process.env.GOOGLE_CLIENT_ID,
//         clientSecret: process.env.GOOGLE_CLIENT_SECRET
//       },
//       function(accessToken, refreshToken, profile, done) {
//         Users.findOrCreate({ where: { google_id : profile.id }, defaults: {
//             google_id: profile._json.id,
//             email: profile._json.email, 
//             first_name: profile._json.given_name,
//             last_name: profile._json.family_name
//         } }).spread((user, created) => {
//             if (!user.blocked) {
//                 return done(false, user, profile);
//             } else {
//                 return done(null, null, {
//                     message: "This User is blocked From using this service!"
//                 });    
//             }
//         }).throw(err => {
//             return done(null, null, {
//                 message: "google auth failed!"
//             });
//         });
//       }
//     ));
// },

//  localStrategy: function(passport) {
//      passport.use(new LocalStrategy({
//         usernameField: 'phone_number'
//      }, 
//      function (user, pass, done) {
//         const number = phoneUtil.parse(user);
//         user = phoneUtil.format(number, PNF.E164);
//          reps.findOne({where : {phone_number: user}}).then(rep => {
//              if (!rep) {
//                  return done(null, null, {
//                      message: 'user not found!'
//                  });
//              }
//              rep.validPassword(pass, function(valid){
//                 if (!valid) {
//                     return done(null, null, {
//                         message: 'Password is wrong'
//                     });
//                 }
//                 // If credentials are correct, return the user object
//                 return done(null, rep);
//              });
//          }).throw(err => {
//              done(err);
//          });
//      }
//      ));
//  },

//  adminStrategy: function(passport) {
//     passport.use('admin-local', new LocalStrategy({
//        usernameField: 'email'
//     }, 
//     function (user, pass, done) {
//         admins.findOne({where : {email: user}}).then(admin => {
//             if (!admin) {
//                 return done(null, null, {
//                     message: 'user not found!'
//                 });
//             }
//             admin.validPassword(pass, function(valid){
//                if (!valid) {
//                    return done(null, null, {
//                        message: 'Password is wrong'
//                    });
//                }
//                // If credentials are correct, return the user object
//                return done(null, admin);
//             });
//         }).throw(err => {
//             done(err);
//         });
//     }
//     ));
//   }
// }
// // module.exports = function(passport){
// //     var opts = {};
// //     opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
// //     opts.secretOrKey = process.env.JWT_ENCRYPTION;

// //     passport.use(new Strategy(opts, (jwt_payload, done) => {
// //         Users.findById(jwt_payload.user_id).then(user => {
// //             if(user) {
// //                 return done(null, user);
// //             }else{
// //                 return done(null, false);
// //             }
// //         });
// //     }));
// // }